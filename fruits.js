db.fruits.insertMany([
    {
      name : "Apple",
      color : "Red",
      stock : 20,
      price: 40,
      supplier_id : 1,
      onSale : true,
      origin: [ "Philippines", "US" ]
    },
  
    {
      name : "Banana",
      color : "Yellow",
      stock : 15,
      price: 20,
      supplier_id : 2,
      onSale : true,
      origin: [ "Philippines", "Ecuador" ]
    },
  
    {
      name : "Kiwi",
      color : "Green",
      stock : 25,
      price: 50,
      supplier_id : 1,
      onSale : true,
      origin: [ "US", "China" ]
    },
  
    {
      name : "Mango",
      color : "Yellow",
      stock : 10,
      price: 120,
      supplier_id : 2,
      onSale : false,
      origin: [ "Philippines", "India" ]
    }
  ]);

//   [Section] MongoDB Aggregation

/* 
    mongoDB aggregation
    -used to generate manipulated data and perform operations to create filtered results taht would help us eventually in analyzing data
    -compared to doing CRUD operations, aggregation gives us access to manipulate, filter, and compute for results providing us to with info to make necessary development decisions to even without having to create front end applications.
*/

// Using the aggregate method
/* 
    Snatch is used to pass the documents that meet the specified decisions to the next pipeline stage/aggregation process
    syntax:
    { $match: {field:value}}
*/

db.fruits.aggregate([
    {$match: {onSale:true}}
]);

//$group is used to group elements together and field-value pairs using the data from the grouped elements

/* 
    Syntax:
    {$group: {_id:"value", fieldResult:"valueResult"}}
*/

db.fruits.aggregate([
    {$group: {_id:"$supplier_id", total:{$sum: "$stock"}}}
]);

// Aggregate Documents using two pipeline stages

/* 
    Syntax:
    db.collectionName.aggregate([
        {$match: {field:value}},
        {$group: {_id:"value", fieldResult:"valueResult"}}
    ])
*/

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum: "$stock"}}}
]);

// Field Projection with aggregation
/* 
    "$project" - it can be used in aggregating data to include/exclude fields from the returned results
    the field that is set to zero(0) will be executed
    the field that is set to one(1) will be the only one included

    syntax:
    {$project: {field:1/0}}
*/

db.fruits.aggregate([
    {$project: {_id: 0}}
])

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum: "$stock"}}},
    {$project: {_id: 0}}
]);

// Sorting aggregated results
/* 
    $sort can be used to change the order of the aggregated results
    -providing a value of -1 will sort the aggregated results in a reverse order
    
    Syntax:
    {$sort{field:1/-1}}
*/

db.fruits.aggregate([
    {$match: {onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum: "$stock"}}},
    {$sort:{total:-1}}
]);

// The field that is set to a value of negative one (-1) was sorted in descending order 
// The field that is set to a value of negative one (1) was sorted in ascending order

// aggregating results based on the array fields
/* 
    $unwind
    deconstructs an array fields from a collection with an array value to give us a result for each array elements

    Syntax:
    {$unwind:field}
*/

db.fruits.aggregate([
    {$unwind:"$origin"}
]);

// Display fruits by their origin and the kinds of fruits that are supplied
db.fruits.aggregate([
    {$unwind:"$origin"},
    {$group:{_id:"$origin", kinds:{$sum:1}}}
]);